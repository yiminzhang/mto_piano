﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonManager : MonoBehaviour {

    public GameObject[] buttons;
    public AudioClip note1, note2, note3, note4, note5, note6, note7, note8;
    public AudioSource buttonAudioSource;
    
    //private int[] buttonOrder = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 3, 3, 3, 4, 5, 5, 5, 6, 7, 7, 8, 1};
    private int[] buttonOrder = {1, 3, 1, 7, 3, 1, 2, 3, 7, 5};

    private int currentButton = 0;
    private int buttonIncrement = 0;
    //private Hashtable hashMap;
    //public LevelManager_Runaway lm;
    //public GameObject emitter;
    public Material defaultMaterial;
    public Material selectMaterial;
    public GameObject leftHand;
    public GameObject rightHand;

	// Use this for initialization
	void Start () {
        buttonIncrement = 0;
        currentButton = buttonOrder[buttonIncrement];
        /*for(int i = 1; i < 9; i++)
        {
            hashMap.Add("note" + System.Convert.ToString(i), buttons[i-1]);
        }*/

        //TODO: change material of currentButton
       // emitter.SetActive(false);
        buttons[buttonIncrement].GetComponent<Renderer>().material = selectMaterial;

    }

    public void SetCurrentButton(int buttonNum)
    {
        print("triggered:" + buttonNum);
        if(currentButton == buttonNum)
        {
            //cast object from hash table to audiosource
            //buttonAudioSource.clip = hashMap["note" + System.Convert.ToString(buttonNum)] as AudioClip;
            print("current button: " + currentButton);
            /*
            if(buttonNum == 1)
            {
                buttonAudioSource.clip = note1;
            }
            else if (buttonNum == 2)
            {
                buttonAudioSource.clip = note2;
            }
            else if (buttonNum == 3)
            {
                buttonAudioSource.clip = note3;
            }
            else if (buttonNum == 4)
            {
                buttonAudioSource.clip = note4;
            }
            else if (buttonNum == 5)
            {
                buttonAudioSource.clip = note5;
            }
            else if (buttonNum == 6)
            {
                buttonAudioSource.clip = note6;
            }
            else if (buttonNum == 7)
            {
                buttonAudioSource.clip = note7;
            }
            else if (buttonNum == 8)
            {
                buttonAudioSource.clip = note8;
            }*/


            switch (buttonNum){
            	case 1:
            		buttonAudioSource.clip = note1;
            		break;
            	case 2:
            		buttonAudioSource.clip = note2;
            		break;
            	case 3:
            		buttonAudioSource.clip = note3;
            		break;
            	case 5:
            		buttonAudioSource.clip = note5;
            		break;
            	case 7:
            		buttonAudioSource.clip = note7;
            		break;
            	default:
            		buttonAudioSource.clip = note1;
            		break;

            };
            StartCoroutine(ButtonPressed());
        }
    }

    IEnumerator ButtonPressed()
    {
        print("button pressed");
        buttonAudioSource.Play();
   
        //TODO: change material of old currentButton back
        buttons[currentButton-1].GetComponent<Renderer>().material = defaultMaterial;
        buttonIncrement++;

        

     
        if(buttonIncrement > 27)
        {
            buttons[currentButton - 1].GetComponent<Renderer>().material = defaultMaterial;
        }

        //TODO: change material of new currentButton
        buttons[currentButton - 1].GetComponent<Renderer>().material = selectMaterial;
        yield return new WaitForSeconds(1f);
    }

    IEnumerator ActivateColliders()
    {
        yield return new WaitForSeconds(2f);
        leftHand.GetComponent<Collider>().enabled = true;
        rightHand.GetComponent<Collider>().enabled = true;
    }
    // Update is called once per frame
    void Update () {
        
	}
}